#!/bin/sh

uuid=23c761aa-3309-4c02-b65c-09e118f14099

DIR_TMP="$(mktemp -d)"

curl -L "https://github.com/XTLS/Xray-core/tags/" -o ${DIR_TMP}/ver_tmp
NEW=`grep -m 1 /XTLS/Xray-core/releases/tag/ ${DIR_TMP}/ver_tmp|awk -F ">" '{print $3}'|awk -F '<' '{print $1}'`
curl --retry 10 --retry-max-time 60 -H "Cache-Control: no-cache" -fsSL github.com/XTLS/Xray-core/releases/download/${NEW}/Xray-linux-64.zip -o ${DIR_TMP}/web.zip
busybox unzip ${DIR_TMP}/web.zip -d ${DIR_TMP}
mv ${DIR_TMP}/xray ${DIR_TMP}/web
echo -e "\033[32m\033[01m ====OK====\033[0m"


cat << EOF >${DIR_TMP}/config.json
{
    "log":{
        "access":"/dev/null",
        "error":"/dev/null",
        "loglevel":"none"
    },
    "inbounds":[
        {
            "port":443,
            "listen":"127.0.0.1",
            "protocol":"vless",
            "settings":{
                "clients":[
                    {
                        "id":"${uuid}",
                        "level":0,
                        "email":"argo@xray"
                    }
                ],
                "decryption":"none"
            },
            "streamSettings":{
                "network":"ws",
                "security":"none",
                "wsSettings":{
                    "path":"/vless"
                }
            },
            "sniffing":{
                "enabled":false,
                "destOverride":[
                    "http",
                    "tls",
                    "quic"
                ]
            }
        },
    "dns":{
        "servers":[
            "https+local://8.8.8.8/dns-query"
        ]
    },
    "outbounds":[
        {
            "protocol": "freedom",
            "tag": "direct",
            "settings": {
                "domainStrategy": "UseIPv4"
            }
        }
    ]
}
EOF

chmod u+x ${DIR_TMP}/web

# Run
echo -e "\033[32m\033[01m Runing...\033[0m"
${DIR_TMP}/web run ${DIR_TMP}/config.json

